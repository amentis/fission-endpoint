extern crate base64;
use std::env;
use std::io::{self, Read, Write};
use std::process::exit;

#[derive(Debug)]
pub enum Method { Get, Post, Put, Patch, Delete }

#[derive(Debug)]
pub enum Authorization {
    Basic {username:String, password: String},
    Bearer(String),
    Other(String)
}

#[inline]
pub fn get_header(key: &str) -> Option<String> {
    env::var(&format!("HTTP_{}",key)).ok()
}

#[inline]
pub fn get_post_data() -> Option<String> {
    let mut buff = String::new();
    match io::stdin().read_to_string(&mut buff) {
        Ok(_) => Some(buff),
        Err(e) => {
            eprintln!("{}", e);
            None
        }
    }
}

#[inline]
pub fn get_authorization() -> Option<Authorization> {
    if let Some(header) = get_header("AUTHORIZATION") {
        Some(match &header.split(' ').collect::<Vec<&str>>()[..] {
            &[method, body] => match method {
                "Basic" => {
                    let raw = base64::decode(body).unwrap();
                    let raw = String::from_utf8(raw).unwrap();
                    let mut auth = raw.split(' ').collect::<Vec<_>>();
                    assert!(auth.len() == 2);
                    let password = auth.pop().unwrap().to_owned();
                    let username = auth.pop().unwrap().to_owned();
                    Authorization::Basic {username, password}
                },
                "Bearer" => Authorization::Bearer(body.to_owned()),
                _ => Authorization::Other(header)
            },
            _ => Authorization::Other(header)
        })
    } else {
        None
    }
}

#[inline]
pub fn get_param(param: &str) -> Option<String> {
    get_header(&format!("X-FISSION-PARAMS-{}", param.to_uppercase()))
}

#[inline]
pub fn endpoint<F,R,E>(handler: F) where R: ToString + Sized, E: ToString + Sized, F: FnOnce() -> Result<R, E> {
    let (response, exit_code) = match handler() {
        Ok(s) => (s.to_string(), 0),
        Err(e) => (e.to_string(), 1)
    };
    io::stdout().write(&response.into_bytes()[..]).unwrap();
    exit(exit_code);
}

#[inline]
pub fn endpoint_raw<F>(handler: F) where F: FnOnce() -> (Vec<u8>, i32) {
    let (response, exit_code) = handler();
    io::stdout().write(&response[..]).unwrap();
    exit(exit_code);
}

